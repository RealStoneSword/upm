# upm

A package manager that allows the user to either compile packages from source or download the binary. upm is inspired by portage and pacman. You can find the main repo [here](https://gitlab.com/RealStoneSword/upm-repo) and an example package [here](https://gitlab.com/RealStoneSword/upm-example-package).