#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>

/* UPM is a minimalist package manager
that can work with both binaries and 
building programs from source. */

/* FILES */

/* /etc/upm/upm.conf - Main configuration file */
/* /etc/upm/repos.conf - Repository configuration file - Add repos here */
/* /lib/ - Library location - This can be changed in upm.conf */
/* /usr/bin - Binary location - This can be changed in upm.conf */

/* If needed, change config location here 
Make sure you keep the trailing forward slash! */
#define CONF_LOCATION "/etc/upm/"
#define REPO_CONFIG_LOCATION CONF_LOCATION "repos.conf"
#define UPM_CONFIG_LOCATION CONF_LOCATION "upm.conf"
#define LIB_LOCATION "/lib/"
#define BIN_LOCATION "/usr/bin/"
/* TODO - Find how manpages work and 
where they go, and setup doc_location */
#define DOC_LOCATION "./doc/"

void die(char *message, int exitcode) {
    printf("%s\n", message);
    exit(exitcode);
}

int check_configs() {
    struct stat configs_stat;
    if (stat(CONF_LOCATION, &configs_stat) == -1) {
        int result = mkdir(CONF_LOCATION, 0777);
        if (result != 0) {
            printf("An error occured while making the directory.\n Try running as root\n");
            exit(1);
        }
    }
    // FILE *fp;
    // fopen(repo_conf_location, "w");
    return 0;
}

void usage() {
    puts(
        "Usage: upm [OPTIONS]\n"
        "\n"
        "Options:\n"
        "--help         Shows this message\n"
        "-i PACKAGE     Builds and Installs a package\n"
        "-b PACKAGE     Installs the binary version of the package\n"
        "-r PACKAGE     Removes a package\n"
        "-l             Lists all installed packages\n"
        "-L             Lists all packages avalible to be installed\n"
        "-u             Updates all installed packages\n"
        "-s             Syncs with added repositories\n"
        "-a URL         Add a git repository\n"
        "-d NAME        Deletes a repository\n"
        "-g             Lists all added repositorys"
    );
}

int main(int argc, char *argv[]) {
    if(argc == 1) {
        printf("missing or unknown operand\ntry --help\n");
        exit(1);
    }
    if(strcmp(argv[1], "--help") == 0) {
        usage();
        exit(0);
    }
    int option;
    while((option = getopt(argc, argv, ":i:b:r:lLusa:d:g")) != -1) {
        switch(option) {
            case 'i':
                printf("build from source and install %s\n", optarg);
                break;
            case 'b':
                printf("install binary version of %s\n", optarg);
                break;
            case 'r':
                printf("remove %s\n", optarg);
                break;
            case 'l':
                printf("list installed packages\n");
                break;
            case 'L':
                printf("list all packages avalible for install\n");
                break;
            case 'u':
                printf("update all packages\n");
                break;
            case 's':
                printf("sync all repos\n");
                break;
            case 'a':
                printf("add a git repo\n");
                break;
            case 'd':
                printf("delete a git repo\n");
                break;
            case 'g':
                printf("list all enabled repos\n");
                break;
            case ':':
                die("Option requires argument", 1);
                break;
            case '?':
                die("missing or unknown operand\ntry --help", 1);
                break;
        }
    }
    return 1;
}