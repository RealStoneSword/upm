COMPILER=gcc
FLAGS=-Werror -Wall -Wextra -pedantic-errors -std=c99
TARGET=upm
SOURCE=main.c

all:
	$(COMPILER) $(FLAGS) -o $(TARGET) $(SOURCE)
clean:
	rm -f $(TARGET)
	$(COMPILER) $(FLAGS) -o $(TARGET) $(SOURCE)